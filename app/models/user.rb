class User < ApplicationRecord
  has_many :user_events
  has_many :events, through: :user_events
  has_many :sent_invites, foreign_key: "inviter_id", class_name: "Invitation"
  has_many :received_invites, foreign_key: "invitee_id", class_name: "Invitation"
end
