module EventHelper
  def parse_event(event)
    {
      :name => event.name,
      :facebook => event.facebook_id,
      :description => event.description,
      :start_at => event.start_at,
      :end_at => event.end_at,
      :cover => event.cover,
      :owner => {
        :id => event.owner_id,
        :name => event.owner_name
      },
      :type => event.type,
      :category => event.category,
      :ticket_uri => event.ticket_uri,
      :sponsored => event.sponsored,
      :city => event.city,
      :place => event.place,
      :location => {
        :longitude => event.longitude,
        :latitude => event.latitude
      }
    }
  end
end
