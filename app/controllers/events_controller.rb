class EventsController < ApplicationController
  before_action :set_event, only: [:show, :update, :destroy, :get_total_accepted_invites]

  # GET /events
  def index
    @events = Event.all

    render json: parse_events(@events)
  end

  # GET /events/all
  def all
    query = [
      "start_time >= ? AND end_time <= ? AND event_type != ?",
      DateTime.now,
      DateTime.now + 90.days,
      ""
    ]
    @events = Event.where(query).order(:start_time => :asc)

    render json: parse_events_all(@events)
  end

  # GET /events/1
  def show
    render json: parse_event(@event)
  end

  # POST /events
  def create
    description = params[:page][:description] || ""
    start_date = DateTime.parse(params[:page][:start_at]) || DateTime.now
    end_date = DateTime.parse(params[:page][:end_at]) || DateTime.now
    local = params[:page][:place] || ""
    owner_id = params[:page][:owner] != '{}' ? JSON.parse(params[:page][:owner])["id"] : ''
    owner_name = params[:page][:owner] != '{}' ? JSON.parse(params[:page][:owner])["name"] : ''
    longitude = params[:page][:location] != '{}' ? JSON.parse(params[:page][:location])["longitude"] : ''
    latitude = params[:page][:location] != '{}' ? JSON.parse(params[:page][:location])["latitude"] : ''

    @event = Event.new(
      :name => params[:page][:name],
      :facebook_id => params[:page][:facebook],
      :description => description,
      :start_time => start_date,
      :end_time => end_date,
      :cover => params[:page][:cover],
      :owner_id => owner_id,
      :owner_name => owner_name,
      :event_type => params[:page][:type],
      :ticket_uri => params[:page][:ticket_uri],
      :sponsored => params[:page][:sponsored],
      :city => params[:page][:city],
      :place => params[:page][:place],
      :longitude => longitude,
      :latitude => latitude
    )

    # @event = Event.new(event_params)

    if (Event.exists?(:facebook_id => @event.facebook_id))
      @event_previous_data = Event.find_by(:facebook_id => @event.facebook_id)
      @event.update(parse_event_to_update(@event_previous_data))
      render json: { :id => @event.facebook_id }
    else
      if @event.save
        render json: { :id => @event.facebook_id }
      else
        render json: @event.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /events/1
  def update
    if @event.update(event_params)
      render json: helpers.parse_event(@event)
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  def sponsored
    @sponsored = Event.where(:sponsored => true)
    # render json: @sponsored
    render json: parse_events(@sponsored)
  end

  # DELETE /events/1
  def destroy
    @event.destroy
  end

  def get_total_accepted_invites
    @total = {}
    @event.users.each { |u|
      @total[u.id] = u
      @total[u].class.module_eval { attr_accessor :count}
      @total[u].count = u.sent_invites.select {|e| e.accepted}.count
    }

    puts @total
    render json: @total
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def event_params
      params.require(:event).permit(:facebook_id, :url, :name, :description, :sponsored, :premium, :start, :end, :local, :longitude, :latitude)
    end

    def parse_events(events)
      events.map do |event|
        parse_event(event)
      end
    end

    def parse_events_all(events)
      events.map do |event|
        parse_event_all(event)
      end
    end

    def parse_event(event)
      { 
        :page => {
          :id => event.id,
          :name => event.name,
          :facebook => event.facebook_id,
          :description => event.description,
          :start_at => event.start_time,
          :end_at => event.end_time,
          :cover => event.cover,
          :owner => {
            :id => event.owner_id,
            :name => event.owner_name
          },
          :type => event.event_type,
          :ticket_uri => event.ticket_uri,
          :sponsored => event.sponsored,
          :city => event.city,
          :place => event.place,
          :location => {
            :longitude => event.longitude,
            :latitude => event.latitude
          },
          :category => {
            :id => 1,
            :description => 'Principal'
          }
        }
      }
    end

    def parse_event_all(event)
      {
        :id => event.id,
        :name => event.name,
        :facebook => event.facebook_id,
        :description => event.description,
        :start_at => event.start_time,
        :end_at => event.end_time,
        :cover => event.cover,
        :owner => {
          :id => event.owner_id,
          :name => event.owner_name
        },
        :type => event.event_type,
        :ticket_uri => event.ticket_uri,
        :sponsored => event.sponsored,
        :city => event.city,
        :place => event.place,
        :location => {
          :longitude => event.longitude,
          :latitude => event.latitude
        },
        :category => {
          :id => 1,
          :description => 'Principal'
        }
      }
    end

    def parse_event_to_update(event)
      {
        name: event.name,
        description: event.description,
        start_time: event.start_time,
        end_time: event.end_time,
        cover: event.cover,
        owner_id: event.owner_id,
        owner_name: event.owner_name,
        event_type: event.event_type,
        ticket_uri: event.ticket_uri,
        sponsored: event.sponsored,
        city: event.city,
        place: event.place,
        longitude: event.longitude,
        latitude: event.latitude
      }
    end
end
