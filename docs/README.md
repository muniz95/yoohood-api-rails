# Instruções

O modelo de dados é gerado utilizando a ferramenta [Draw.io](http://draw.io), a qual é gratuita e online. Para alterar o DER basta logar no Draw.io e, através dele, abrir o arquivo *DER-Yoohood.xml*.

Sempre que o DER for alterado, preferencialmente gere uma imagem do modelo para referência rápida. Para exportar como imagem, dentro do draw.io acesse o menu

**File \> Export as \> PNG... \> Export**

e salve o arquivo na pasta *docs/* na raiz do projeto.