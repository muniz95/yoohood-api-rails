require 'active_support/all'

module Seed
  Event.create([
    {
      name: 'Evento teste 1',
      description: 'Evento criado para ser o número 1 na lista',
      start: Date.today + 2.weeks,
      end: Date.today + 2.weeks + 2.days,
    },
    {
      name: 'Evento teste 2',
      description: 'Evento criado para ser o número 2 na lista',
      start: Date.today + 2.weeks,
      end: Date.today + 2.weeks + 2.days,
    },
    {
      name: 'Evento teste 3',
      description: 'Evento criado para ser o número 3 na lista',
      start: Date.today + 2.weeks,
      end: Date.today + 2.weeks + 2.days,
    },
    {
      name: 'Evento teste 4',
      description: 'Evento criado para ser o número 4 na lista',
      start: Date.today + 2.weeks,
      end: Date.today + 2.weeks + 2.days,
    },
    {
      name: 'Evento teste 5',
      description: 'Evento criado para ser o número 5 na lista',
      start: Date.today + 2.weeks,
      end: Date.today + 2.weeks + 2.days,
    }
  ])
end