require 'active_support/all'

module Seed
  usuario = User.first
  categoria = Category.find(7) # Rave
  BlacklistedCategory.create([
    { user: usuario, category: categoria }
  ])
end