class AddMissingFieldsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :cover, :string
    add_column :events, :owner_name, :string
    add_column :events, :owner_id, :integer, :limit => 8
    add_column :events, :type, :string
    add_column :events, :ticket_uri, :string
    add_column :events, :city, :string
    add_column :events, :place, :string
  end
end
