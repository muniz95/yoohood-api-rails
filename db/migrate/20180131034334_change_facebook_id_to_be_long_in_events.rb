class ChangeFacebookIdToBeLongInEvents < ActiveRecord::Migration[5.1]
  def change
    change_column :events, :facebook_id, :integer, limit: 8
  end
end
