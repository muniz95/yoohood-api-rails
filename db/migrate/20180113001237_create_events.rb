class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.integer :facebook_id
      t.string :url
      t.string :name
      t.string :description
      t.boolean :sponsored
      t.boolean :premium
      t.date :start
      t.date :end
      t.string :local
      t.string :longitude
      t.string :latitude

      t.timestamps
    end
  end
end
