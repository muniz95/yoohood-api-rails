# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require File.expand_path('../seed/category', __FILE__)
# require File.expand_path('../seed/event', __FILE__)
# require File.expand_path('../seed/user', __FILE__)
require File.expand_path('../seed/blacklisted_category', __FILE__)
require File.expand_path('../seed/event_category', __FILE__)