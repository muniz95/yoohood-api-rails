namespace :db do
  desc 'This task rebuilds the development db'
  task :rebuild, [] => :environment do
    raise "This task runs in development or staging only" if Rails.env.production?

    Rake::Task['db:drop'].execute
    Rake::Task['db:create'].execute
    Rake::Task['db:migrate'].execute
    # Rake::Task['db:seed'].execute
  end
end